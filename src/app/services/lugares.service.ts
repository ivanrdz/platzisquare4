import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable()
export class LugaresService {

  lugares: any = [
    {id: 1, plan: 'pagado', cercania: 1, distancia: 1, active: true, nombre: 'Floreria 1'},
    {id: 2, plan: 'gratuito', cercania: 1, distancia: 1.8, active: false, nombre: 'Donas 1'},
    {id: 3, plan: 'gratuito', cercania: 2, distancia: 10, active: true, nombre: 'Veterinaria 1'},
    {id: 4, plan: 'pagado', cercania: 3, distancia: 35, active: false, nombre: 'Floreria 2'},
    {id: 5, plan: 'gratuito', cercania: 3, distancia: 105, active: true, nombre: 'Donas 2'},
    {id: 6, plan: 'pagado', cercania: 3, distancia: 160, active: false, nombre: 'Veterinaria 2'},
    {id: 7, plan: 'pagado', cercania: 3, distancia: 105, active: true, nombre: 'Donas Ricas 2'},
    {id: 8, plan: 'pagado', cercania: 3, distancia: 160, active: true, nombre: 'Veterinaria de animales 2'}
  ];
  constructor(private afDB: AngularFireDatabase) {}

  public getLugares() {
    return this.afDB.list('lugares/');
  }
  public buscarLugar(id) {
    return this.lugares.filter((lugar) => lugar.id == id)[0] || null;
  }
  public guardarLugar(lugar) {
    console.log(lugar);
    this.afDB.database.ref('lugares/' + lugar.id).set(lugar);
    // this.afDB.database.ref --- es lo necesario para hacer la peticion
  }

}
