import { Component, OnInit } from '@angular/core';
import { LugaresService } from '../services/lugares.service';

@Component({
  selector: 'app-lugares',
  templateUrl: './lugares.component.html',
  styleUrls: ['./lugares.component.css']
})
export class LugaresComponent implements OnInit {
  lat: number = 25.7250041;
  lng: number = -100.3156047;
  lugares = null;

  constructor(private lugaresService: LugaresService) {
    lugaresService.getLugares()
      .valueChanges().subscribe(lugares =>{
        this.lugares = lugares;
      });
  }

  ngOnInit() {
  }

}
